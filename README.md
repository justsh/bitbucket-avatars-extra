Bitbucket Avatars
-----------------

This is my set of extra avatars for Atlassian Bitbucket

Originals done by Bitbucket redesigner [Joel Unger](http://dribbble.com/joelunger) as seen [here](http://dribbble.com/shots/763075-Bitbucket-default-repository-avatars)
